class LoginForm extends React.Component{
    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
    }

    render() {
        return(

            <form className="login_form" id="login-form" onSubmit={this.handleLogin}>
                <label>ایمیل:</label>
                <input type="text" name="std_id" value="" /><br/><br/>
                <label>رمز عبور:</label>
                <input type="password" name="pass" value="" /><br/><br/>
                <button type="submit">ورود</button>
            </form>

        );
    }

    handleLogin() {
        event.preventDefault();
        // var params = {
        //     "std_id": this.props.std_id,
        //     "password" : this.props.password,
        // };
        // var queryString = Object.keys(params).map(function(key) {
        //     return key + '=' + params[key]
        // }).join('&');
        // const requestOptions = {
        //     method: 'POST',
        //     headers: {
        //         'content-length' : queryString.length,
        //         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        //     },
        //     body: queryString
        // };
        const form = new FormData(document.getElementById('login-form'));
        fetch('/login', {
            method: 'POST',
            body: form
        });

        // fetch('/', requestOptions)
        //     .then(response => response.json());
        //     // .then(data => $("#infoModal").modal("hide"));
    }

}