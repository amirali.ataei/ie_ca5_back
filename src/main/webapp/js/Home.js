class Info extends React.Component { //props have to be a Student.
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <>
                <img id="profile" src={this.props.getImg()} />
                <table>
                    <tr>
                        <td> نام: </td>
                        <td> {this.props.getName()} </td>
                    </tr>
                    <tr>
                        <td> نام خانوادگی: </td>
                        <td> {this.props.getSecondName()} </td>
                    </tr>
                    <tr>
                        <td> شماره دانشجویی: </td>
                        <td> {this.props.getStudentId()} </td>
                    </tr>
                    <tr>
                        <td> تاریخ تولد: </td>
                        <td> {this.props.getBirthDate()} </td>
                    </tr>
                    <tr>
                        <td> معدل کل: </td>
                        <td> {this.props.getGPA()} </td>
                    </tr>
                    <tr>
                        <td> واحد گذرانده: </td>
                        <td> {this.props.getNumOfPassedUnits()} </td>
                    </tr>

                    <tr>
                        <td> دانکشده: </td>
                        <td> {this.props.getFaculty()} </td>
                    </tr>
                    <tr>
                        <td> رشته: </td>
                        <td> {this.props.getField()} </td>
                    </tr>
                    <tr>
                        <td> مقطع: </td>
                        <td> {this.props.getLevel()} </td>
                    </tr>
                </table>
                <p id="status">{this.props.getStatus()}</p>
            </>
        );
    }
}

class CourseInReport extends React.Component { //props have to be a Grade.
    constructor(props, _i) {
        super(props);
        this.i = _i;
    }

    render() {
        return (<tr>
            <td>{this.i}</td>
            <td>{this.props.getCode()}</td>
            <td>{this.props.getName()}</td>
            <td>{this.props.getUnits()} واحد</td>
            <td><p class="pass">{this.props.getStatus()}</p></td>
            <td class="pass2">{this.props.getGrade()}</td>
        </tr>);
    }
}

class TermInReport extends React.Component{ //props have to be a Term.
    constructor(props, _i) {
        super(props);
        this.i = _i;
    }

    render() {
        return(
            props.getGrades().map((grade) => <Report />)
        );
    }
    const rows = [];

    for(var j = 0; j < props.getGrades().size(); j++) {
        rows.push(report(props, i, j));
    }
    return rows;
}

function report(props, i, j) {
    return (<>
        <legend> {i} ترم </legend>
        <table>
            {courseInReport(props.getGrades().get(j), {j})}
            <p class="avg">معدل: {props.getAvg()}</p>
        </table>
    </>);
}

class Report extends React.Component{ //props have to be a Student.
    constructor(props) {
        super(props);
    }

    fetchAvailable() {
        fetch('/' + this.props.destination)
            .then(resp => resp.json())
            .then(data => this.setState(prevState => ({ available : data.available })));
    }

    var i;
    var res = (<legend> کارنامه-ترم {this.i} </legend>);
    for(i = 12; i > 0 && props.getTerms()[i].getCourses().size() > 0; i--){
        res = res + termInReport(props.getTerms()[i], i);
    }


    render() {

    }
    return res;
}