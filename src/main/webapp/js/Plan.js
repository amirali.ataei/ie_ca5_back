function planTableRow(props,i){
    return(
        <>
            <td>{props.sat[i]}</td>
           <td>{props.sun[i]}</td>
           <td>{props.mon[i]}</td>
           <td>{props.tue[i]}</td>
           <td>{props.wed[i]}</td>
           <td></td>
            </>
    );
}

function planTable(props){ //props should be same as PlanServlet
        return(<table class="plan">
                   <tr>
                       <th></th>
                       <th>شنبه</th>
                       <th>یک‌شنبه</th>
                       <th>دوشنبه</th>
                       <th>سه‌شنبه</th>
                       <th>چهارشنبه</th>
                       <th>پنج‌شنبه</th>
                   </tr>
                   <tr>
                       <td>۷:۳۰ - ۹:۰۰</td>
                       {planTableRow(props,0)}
                   </tr>
                   <tr>
                       <td>۹:۰۰ - ۱۰:۳۰</td>
                       {planTableRow(props,1)}
                   </tr>
                   <tr>
                       <td>۱۰:۳۰ - ۱۲:۰۰</td>
                       {planTableRow(props,2)}
                   </tr>
                   <tr>
                       <td>۱۴:۰۰ - ۱۵:۳۰</td>
                       {planTableRow(props,3)}
                   </tr>
                   <tr>
                       <td>۱۶:۰۰ - ۱۷:۳۰</td>
                       {planTableRow(props,4)}
                   </tr>
               </table>);
}
