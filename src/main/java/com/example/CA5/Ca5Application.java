package com.example.CA5;

import com.example.CA5.controllers.HomeController;
import com.example.CA5.controllers.models.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

import static com.example.CA5.controllers.models.Tools.*;


@SpringBootApplication
public class Ca5Application {

	public static void main(String[] args) {
		try {
			courses = getCourses();
			students = getStudents();
			for(Student student:students) {
				student.initialTerms();
				ArrayList<Grade> grades = getGrades(student.getStudentId());
				for (Grade course : grades) {
					student.addCourse(course);
				}
			}
			System.out.println("Hello");
		}
		catch (Exception e) {}

		SpringApplication.run(HomeController.class, args);
	}

}
