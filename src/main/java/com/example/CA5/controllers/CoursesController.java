package com.example.CA5.controllers;

import com.example.CA5.controllers.models.Course;
import com.example.CA5.controllers.models.Datas;
import com.example.CA5.controllers.models.Student;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.example.CA5.controllers.models.Tools.*;
import static java.rmi.server.LogStream.log;


@RestController
@RequestMapping(value = "/courses", produces = MediaType.APPLICATION_JSON_VALUE)

public class CoursesController {

    ArrayList<Course> filterCourses = courses;

    @GetMapping("/{filter}")
    public Datas getData(@PathVariable(value = "filter") String search){
        searchFilter = search;
        filterCourses = filter();
        Datas data = new Datas();
        data.setStatusCode(202);
        return data;
    }

    @PostMapping()
    public Datas postData(
            @RequestParam(value = "course_code") String code,
            @RequestParam(value = "class_code") String classCode,
            @RequestParam(value = "value") String value) {

        if(studentId.equals(""))
        {
            Datas data = new Datas();
            data.setStatusCode(403);
            return data;
        }


        Course course = null;
        try {
            course = getCourse(code, classCode);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Student student = null;
        try {
            student = getStudent(studentId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert student != null;

        filterCourses = filter();

        switch (value) {
            case "wait":

                try {
                    student.addToWeeklySchedule(course);
                    course.setWaiting(true);
                    selectedCourses.add(course);
                    course.setStatusCode(202);
                } catch (Exception e) {
//                    log(e.toString());
                    course.setStatusCode(405);
                }

                break;
            case "add":

                try {
                    student.addToWeeklySchedule(course);
                    selectedCourses.add(course);
                    course.setStatusCode(202);
                } catch (Exception e) {
//                    log(e.toString());
                    course.setStatusCode(405);
                }
                break;

            case "submit":
                try {
                    student.setWeeklySchedule(selectedCourses);
                    student.finalizeWeeklySchedule();
                    lastSubmit = new ArrayList<>(selectedCourses);
                    course.setStatusCode(202);
                } catch (Exception e) {
                    course.setStatusCode(405);
                }
                break;

        }
        return course;
    }

    @DeleteMapping()
    public Course deleteData(
            @RequestParam(value = "course_code") String code,
            @RequestParam(value = "class_code") String classCode) {

        Datas data = new Datas();
        Course course = null;

        Student student = null;
        try {
            student = getStudent(studentId);
        } catch (Exception e) {
            data.setStatusCode(404);
            e.printStackTrace();
        }
        assert student != null;

        try {
            course = getCourse(code, classCode);
        } catch (Exception e) {
            data.setStatusCode(404);
            e.printStackTrace();
        }

        try {
            student.removeFromWeeklySchedule(course);
            selectedCourses.remove(course);
            course.setStatusCode(202);
        } catch (Exception e) {
            course.setStatusCode(405);
            e.printStackTrace();
        }

        return course;
    }

    @PutMapping()
    public Datas putData() {
        Datas data = new Datas();
        Student student = null;
        try {
            student = getStudent(studentId);
        } catch (Exception e) {
            data.setStatusCode(404);
            e.printStackTrace();
        }
        assert student != null;

        student.setWeeklySchedule(lastSubmit);
        selectedCourses = new ArrayList<>(lastSubmit);

        data.setStatusCode(202);
        return data;
    }
}
