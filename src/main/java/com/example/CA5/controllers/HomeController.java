package com.example.CA5.controllers;

import com.example.CA5.controllers.models.Student;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static com.example.CA5.controllers.models.Tools.getStudent;
import static com.example.CA5.controllers.models.Tools.studentId;

@RestController
@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class HomeController {

    @PostMapping()
    public Student postData(
            @RequestParam(value = "std_id") String id,
            @RequestParam(value = "password") String pass){

        studentId = id;
        Student student = null;
        try {
            student = getStudent(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return student;
    }
}
