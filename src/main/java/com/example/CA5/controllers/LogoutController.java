package com.example.CA5.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.example.CA5.controllers.models.Tools.studentId;

@RestController
@RequestMapping(value = "/logout")
public class LogoutController {
    @GetMapping()
    public void getData() {
        studentId = "";
    }
}
