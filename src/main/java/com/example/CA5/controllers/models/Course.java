package com.example.CA5.controllers.models;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import com.example.CA5.controllers.models.Datas;

import static com.example.CA5.controllers.models.Tools.getStudent;

public class Course extends Datas {
    private String code;
    private String classCode;
    private String name;
    private int units;
    private int signedUp = 0;
    private String type;
    private String instructor;
    private int capacity;
    private ArrayList<String> prerequisites;
    private JSONObject classTime;
    private JSONObject examTime;

    private boolean status = false;
    private boolean waiting = false;
    private ArrayList<String> students = new ArrayList<>();
    private ArrayList<String> waitingStudents = new ArrayList<>();

    public Course(JSONObject jsonObject) {
        code = (String) jsonObject.get("code");
        classCode = (String) jsonObject.get("classCode");
        name = (String) jsonObject.get("name");
        instructor = (String) jsonObject.get("instructor");
        units = (int) jsonObject.get("units");
        classTime = (JSONObject) jsonObject.get("classTime");
        examTime = (JSONObject) jsonObject.get("examTime");
        capacity = (Integer) jsonObject.get("capacity");
        JSONArray array = (JSONArray) jsonObject.get("prerequisites");
        for (String s : (Iterable<String>) array) {
            prerequisites.add(s);
        }
    }

    public Course() {
    }

    public int getWaitingStudentsSize() {
        return waitingStudents.size();
    }

    public void popWaiting() {
        String studentId = waitingStudents.remove(0);
        waiting = false;
        status = true;
        addToStudents(studentId);
    }

    public boolean isWaiting() {
        return waiting;
    }

    public void setWaiting(boolean wait){
        waiting = wait;
        capacity += 1;
    }

    public String getCode() {
        return  this.code;
    }

    public String getClassCode() {
        return classCode;
    }


    public String getName()
    {
        return this.name;
    }

    public String getInstructor()
    {
        return this.instructor;
    }

    public int getUnits() { return this.units; }

    public String getType() {
        return type;
    }

    public JSONObject getClassTime(){
        return this.classTime;
    }

    public JSONObject getExamTime() {
        return this.examTime;
    }

    public int getCapacity() { return this.capacity; }

    public ArrayList<String> getPrerequisites() {
        return this.prerequisites;
    }

    public boolean getStatus() {
        return this.status;
    }

    public int getSignedUp() {return this.signedUp;}

    public void addToStudents(String studentId){
        students.add(studentId);
        signedUp = signedUp + 1;
    }

    public void finalizeOffer(String studentId) {
        if(waiting){
            waitingStudents.add(studentId);
            return;
        }

        if(!status) {
            addToStudents(studentId);
            status = true;
        }
    }


    public void removeFromStudents(String studentId) {
        status = false;
        students.remove(studentId);
        signedUp -= 1;
    }

    public String toString() {
        Map file = new LinkedHashMap();
        file.put("code", code);
        file.put("name", name);
        file.put("Instructor", instructor);
        file.put("units", units);
        file.put("classTime", classTime.toJSONString());
        file.put("examTime", examTime.toJSONString());
        file.put("capacity", capacity);
        file.put("prerequisites", prerequisites.toString());

        return JSONValue.toJSONString(file);
    }
}
