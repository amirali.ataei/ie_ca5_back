package com.example.CA5.controllers.models;

import com.example.CA5.controllers.exceptions.*;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class WeeklySchedule extends Datas{
    Student student;
    private ArrayList<Course> courses;
    private int totalUnits;
    private ArrayList<Course> removedCourses;

    public WeeklySchedule(Student stu) {
        courses = new ArrayList<>();
        removedCourses = new ArrayList<>();
        this.totalUnits = 0;
        student = stu;
    }

    public int getTotalUnits() {
        return totalUnits;
    }

    private void checkMinUnits() throws MinimumUnitsException {
        if(this.totalUnits<12)
            throw new MinimumUnitsException();
    }
    private void checkMaxUnits() throws MaximumUnitsException {
        if(this.totalUnits>20)
            throw new MaximumUnitsException();
    }
    private void checkClassTimeCollision(Course _course) throws ClassTimeCollisionException {

        JSONObject _classTime = _course.getClassTime();
        ArrayList<String> _days = (ArrayList<String>) _classTime.get("days");
        String _time = (String) _classTime.get("time");

        for(Course course: courses)
        {
            JSONObject classTime = course.getClassTime();
            ArrayList<String> days = (ArrayList<String>) classTime.get("days");
            String time = (String) classTime.get("time");

            if(days.get(0).equals(_days.get(0)) || days.get(0).equals(_days.get(1)) || days.get(1).equals(_days.get(1)) || days.get(1).equals(_days.get(0))){
                if(time.equals(_time))
                    throw new ClassTimeCollisionException(_course.getCode(), _course.getCode());
            }
        }
    }

    private void checkExamTimeCollision(Course _course) throws ExamTimeCollisionException {

        JSONObject _examTime = _course.getExamTime();
        String _start = (String) _examTime.get("start");
        String _end = (String) _examTime.get("end");

        for(Course course : courses)
        {
            JSONObject examTime = course.getExamTime();
            String start = (String) examTime.get("start");
            String end = (String) examTime.get("end");


            if(start.equals(_start) && end.equals(_end)){
                throw new ExamTimeCollisionException(_course.getCode(), _course.getCode());
            }
        }

    }
    private void checkCapacity() throws CapacityException {
        for(Course course : courses)
        {
            if(course.getSignedUp() > course.getCapacity())
                throw new CapacityException();
        }
    }

    private void checkPrerequisites() throws Exception {
        for(Course course : courses) {
            for(String code : course.getPrerequisites()) {
                if(!student.hadCourse(code))
                    throw new PrerequisitesException(course.getName());
            }
        }
    }

    private void checkPassedCourses() throws Exception {
        for(Course course : courses) {
            if(student.hadCourse(course.getCode()) && student.getGrade(course.getCode()) >= 10.0)
                throw new HadPassedCourseException(course.getName());
        }
    }

    private void check() throws Exception{
        checkMinUnits();
        checkMaxUnits();
        checkPrerequisites();
        checkPassedCourses();
        checkCapacity();
    }

    public void aTWS(Course course) throws Exception{
        this.checkClassTimeCollision(course);
        this.checkExamTimeCollision(course);
        courses.add(course);
        totalUnits = totalUnits + course.getUnits();
    }
    public void rFWS(Course course) {
//        boolean courseFound = false;
//        for(Course _course : courses)
//        {
//            if(_course.getCode().equals(course.getCode()))
//            {
//                courses.remove(_course);
//                totalUnits = totalUnits - course.getUnits();
//                return ;
//            }
//        }
        if(course.getStatus())
            removedCourses.add(course);
        courses.remove(course);
        totalUnits = totalUnits - course.getUnits();
//        throw new OfferingNotFoundException();
    }

    public ArrayList<Course> gWS() {
        return courses;
    }

    public void setWS(ArrayList<Course> selectedCourses) {
        courses = new ArrayList<>(selectedCourses);
        totalUnits = 0;
        for(Course course : courses) {
            totalUnits += course.getUnits();
        }
    }


    public void fWS() throws Exception{
        this.check();
        for(Course course : courses)
            course.finalizeOffer(student.getStudentId());
        for(Course course : removedCourses) {
            course.removeFromStudents(student.getStudentId());
        }
        removedCourses = new ArrayList<>();
    }
}
