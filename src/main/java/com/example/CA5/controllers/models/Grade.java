package com.example.CA5.controllers.models;

import static com.example.CA5.controllers.models.Tools.getCourse;

public class Grade extends Datas{
    private String code;
    private double grade;
    private int term;

    public Grade(String _code, double _grade, int _term) {
        code = _code;
        grade = _grade;
        term = _term;
    }

    public String getCode() {
        return code;
    }

    public double getGrade() {
        return grade;
    }

    public int getTerm() {
        return term;
    }

    public int getUnits() {
        int units = 0;
        try {
            units = getCourse(code, "01").getUnits();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return units;
    }

    public String getName() {
        String name = "";
        try {
            name = getCourse(code, "01").getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }
}
