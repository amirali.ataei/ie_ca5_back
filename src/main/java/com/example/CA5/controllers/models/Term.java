package com.example.CA5.controllers.models;

import java.util.ArrayList;

public class Term extends Datas{
    private ArrayList<Grade> grades = new ArrayList<>();
    private double avg;
    private int number;

    Term(int _number) {
        number = _number;
        avg = 0.0;
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void addGrade(Grade grade){
        grades.add(grade);
    }

    public double getAvg() {
        for(Grade grade: grades){
            avg += grade.getGrade();
        }
        avg /= grades.size();
        return avg;
    }

    public int getNumber() {
        return number;
    }

}
