package com.example.CA5.controllers;


import com.example.CA5.controllers.models.Student;
import org.apache.catalina.filters.RemoteIpFilter;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;

import static com.example.CA5.controllers.models.Tools.getStudent;

@RestController
@RequestMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
public class LoginController {

    @GetMapping("/{id}")
    public Student getData(@PathVariable String id) {
        Student student = null;
        try {
            student = getStudent(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return student;
    }

    @PostMapping()
    public String postData(@RequestBody Student student) {
        return "Hello " + student.getStudentId();
    }
}
